//
//  HFFluidGlass.swift
//  HFFluidGlass
//
//  Created by Dmitry Antonenka on 3/22/17.
//  Copyright © 2017 BSUIR. All rights reserved.
//

import UIKit
import QuartzCore
import GameplayKit

@available(iOS 10.0, *)
@IBDesignable
open class HFFluidGlass: UIView {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor.clear
    }
    
    @IBInspectable open var minValue: CGFloat = 100 {
        didSet {
            setNeedsDisplay()
        }
    }

    @IBInspectable open var maxValue: CGFloat = 500 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable open var currentValue: CGFloat = 400 {
        didSet {
            _currentProgressPercentage = currentValue / maxValue
        }
    }
    
    //========================================================================
    //MARK: IBInspectable properties
    
    @IBInspectable open var edgeColor: UIColor = UIColor.white {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable open var edgeWidth: CGFloat = 3.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable open var topBottomWidth: CGFloat = 2.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    fileprivate var _iterationalProgressPercentage: CGFloat = 0.0
    @IBInspectable open var _currentProgressPercentage: CGFloat = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable open var bubleRadius: CGFloat = 6.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable open var fluidColor: UIColor = UIColor(red: 0.271, green: 0.824, blue: 0.282, alpha: 1.000) {
        didSet {
            setNeedsDisplay()
        }
    }

    @IBInspectable open var shadowYOffset: CGFloat = 62 {
        didSet {
            setNeedsDisplay()
        }
    }

    @IBInspectable open var glassFillMaxYMultiplier: CGFloat = 0.6 {
        didSet {
            setNeedsDisplay()
        }
    }

    @IBInspectable open var animationTime: CFTimeInterval = 0.4
    
    //Fileprivate properties for handling fluid level rendering.
    
    fileprivate var glassTopEllipseYStart: CGFloat = 0.0
    fileprivate var glassTopEllipseYCenter: CGFloat = 0.0
    fileprivate var glassBottomEllipseYCenter: CGFloat = 0.0

    fileprivate var glassOuterBottomEllipseXStart: CGFloat = 0.0
    fileprivate var glassOuterBottomEllipseXEnd: CGFloat = 0.0
    
    fileprivate var glassInnerBottomEllipseXStart: CGFloat = 0.0
    fileprivate var glassInnerBottomEllipseXEnd: CGFloat = 0.0
    fileprivate var glassInnerBottomEllipseHeight: CGFloat = 0.0

    fileprivate var glassFrame: CGRect = .zero
    
    //Fileprivate draw waves properties
    
    fileprivate var timer: Timer?
    fileprivate let scheduleTimerInterval: TimeInterval = 0.01

    fileprivate var yWaveDeviation: CGFloat = 0
    fileprivate var xWaveDeviation: CGFloat = 0
    
    fileprivate let graphWidth: CGFloat = 0.99  // Graph is 80% of the width of the view
    fileprivate let amplitude: CGFloat = 0.05   // Amplitude of sine wave is 30% of view height
    
    fileprivate let waveCount: CGFloat = 1.5
    fileprivate let renderXOffset: CGFloat = 10
    
    
    //MARK: - Interface methods
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupRedrawTimer()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRedrawTimer()
    }
    
    deinit {
        timer?.invalidate()
        timer = nil
    }
    
    override open func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        UIGraphicsBeginImageContext(self.bounds.size)
        
        draw(self.bounds)
        
        UIGraphicsEndImageContext();
    }
    
    override open func draw(_ rect: CGRect) {
        backgroundColor = UIColor.clear

        drawGlassOfFluid(frame: rect)
        drawWaves(frame: rect, newProgress: _iterationalProgressPercentage)
    }
}

// MARK: - Fileprivate methods
//
@available(iOS 10.0, *)
fileprivate extension HFFluidGlass {
    func setupRedrawTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: scheduleTimerInterval, repeats: true, block: { [weak self] (timer) in
            guard let weakSelf = self else { return }
            
            weakSelf.update(withDelta: CACurrentMediaTime())
            weakSelf.setNeedsDisplay()
        })
    }
    
    var y_secsPerMove: CGFloat {
        return 2.5
    }
    var x_secsPerMove: CGFloat {
        return 4.0
    }
    var fillChangeCoeff: CGFloat {
        return 0.005
    }
    
    func update(withDelta delta: TimeInterval) {
        xWaveDeviation = (sin(CGFloat(delta) * 2.0 * CGFloat.pi / x_secsPerMove)) * 0.1
        yWaveDeviation = (sin(CGFloat(delta) * 2.0 * CGFloat.pi / y_secsPerMove)) * 0.05
        
        let targetProgressDiff =  _currentProgressPercentage - _iterationalProgressPercentage
        if round(targetProgressDiff * 100.0)/100.0 != 0 {
            _iterationalProgressPercentage += CGFloat( sign( Double(targetProgressDiff) ) ) * fillChangeCoeff
            //            _iterationalProgressPercentage += CGFloat( CGFloat( sign( Double(targetProgressDiff) ) ) * abs(yWaveDeviation) * 0.05 ) /// experimental version
        } else {
            _iterationalProgressPercentage = _currentProgressPercentage
        }
        
        setNeedsDisplay()
    }
}

// MARK: - Draw help methods
//
@available(iOS 10.0, *)
fileprivate extension HFFluidGlass {

    func toRadians(_ angle: CGFloat) -> CGFloat {
        return (angle/180)*CGFloat.pi
    }
    
    func sinModif(_ angle: CGFloat) -> CGFloat {
        return (0.5 + yWaveDeviation)*sin(angle * (1 + xWaveDeviation))
    }
    
    func fluidColor(_ fillProgress: CGFloat) -> UIColor {
        return fluidColor.withAlphaComponent( max(fillProgress, 0.6) )
    }
    
    func bottonInnerEllipseBeizerPath() -> UIBezierPath {
        let bottonInnerEllipse = UIBezierPath(ovalIn: CGRect(x: glassInnerBottomEllipseXStart, y: glassBottomEllipseYCenter-glassInnerBottomEllipseHeight/2, width: glassInnerBottomEllipseXEnd-glassInnerBottomEllipseXStart, height: glassInnerBottomEllipseHeight))
        return bottonInnerEllipse
    }
    
    func drawWaves(frame rect: CGRect, newProgress: CGFloat) {
        guard newProgress > 0.1 else { return }
        
        ///Glass constants
        let glassHeight = abs(glassTopEllipseYCenter - glassBottomEllipseYCenter)
        let edgeWidth = abs(glassOuterBottomEllipseXStart - glassInnerBottomEllipseXStart)
        
        let deltaY = glassHeight * glassFillMaxYMultiplier
        let deltaX = edgeWidth * glassFillMaxYMultiplier

        let targetYPoint = glassBottomEllipseYCenter - deltaY * (newProgress)
        
        let targetLeftExtreemePoint = CGPoint(x: glassInnerBottomEllipseXStart - deltaX * (newProgress) + ceil(self.edgeWidth/2), y: targetYPoint)
        let targetRightExtreemePoint = CGPoint(x: glassInnerBottomEllipseXEnd + deltaX * (newProgress) - ceil(self.edgeWidth/2), y: targetYPoint)
        
        ///Waves constant
        let width = targetRightExtreemePoint.x - targetLeftExtreemePoint.x
        let height = deltaY
        
        let startPt = targetLeftExtreemePoint
        
        let path = UIBezierPath()
        path.move(to: startPt)
        
        let waveCount = self.waveCount * (1 + xWaveDeviation)
        
        for angle in stride(from: 1.0, through: (360 * waveCount), by: 1.0) {
            var x = startPt.x + CGFloat(angle/360.0) * width * (graphWidth/waveCount)
            x += (1 + xWaveDeviation)
            x = min(max(targetLeftExtreemePoint.x, x), targetRightExtreemePoint.x)
            
            var y = startPt.y - CGFloat( sinModif( toRadians(angle) )) * height * amplitude
            y *= (1 + yWaveDeviation)
            
            path.addLine(to: CGPoint(x: x, y: y))
        }
        
        let glassBottomEllipseRightPoint = CGPoint(x: glassInnerBottomEllipseXEnd, y:glassBottomEllipseYCenter)
        let glassBottomEllipseLeftPoint = CGPoint(x: glassInnerBottomEllipseXStart, y:glassBottomEllipseYCenter)
        
        path.addLine(to: glassBottomEllipseRightPoint)
        path.addLine(to: glassBottomEllipseLeftPoint)
        path.addLine(to: startPt)
        path.append(bottonInnerEllipseBeizerPath())
        
        fluidColor(newProgress).setFill()
        path.fill()
    }

    func drawGlassOfFluid(frame: CGRect = CGRect(x: 5, y: 80, width: 27, height: 59), lineWidth: CGFloat = 1, glassTopWidth: CGFloat = 1, glassOuterBottomWidth: CGFloat = 1) {
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()!
        // This non-generic function dramatically improves compilation times of complex expressions.
        func fastFloor(_ x: CGFloat) -> CGFloat { return floor(x) }
        
        
        //// Shadow Declarations
        let helpShadowOvalShadow = NSShadow()
        helpShadowOvalShadow.shadowColor = UIColor.black.withAlphaComponent(0)
        helpShadowOvalShadow.shadowOffset = CGSize(width: 0, height: shadowYOffset)
        helpShadowOvalShadow.shadowBlurRadius = 2
        let shadow = NSShadow()
        shadow.shadowColor = UIColor.black.withAlphaComponent(0)
        shadow.shadowOffset = CGSize(width: 0, height: shadowYOffset)
        shadow.shadowBlurRadius = 5
        
        //// Subframes
        let glass: CGRect = CGRect(x: frame.minX + fastFloor(frame.width * 0.01686 + 0.04) + 0.46, y: frame.minY + fastFloor(frame.height * 0.00847) + 0.5, width: fastFloor(frame.width * 0.99999 - 0.5) - fastFloor(frame.width * 0.01686 + 0.04) + 0.54, height: fastFloor(frame.height * 0.97458) - fastFloor(frame.height * 0.00847))
        glassFrame = glass
        
        //// Glass
        //// Outer Structure Group
        //// Glass Outer Bottom Drawing
        let glassOuterBottomRect = CGRect(x: glass.minX + fastFloor(glass.width * 0.00169 + 0.46) + 0.04, y: glass.minY + fastFloor(glass.height * 0.84211 + 0.5), width: fastFloor(glass.width * 0.98118 + 0.46) - fastFloor(glass.width * 0.00169 + 0.46), height: fastFloor(glass.height * 1.00000 + 0.5) - fastFloor(glass.height * 0.84211 + 0.5))
        let glassOuterBottomPath = UIBezierPath(ovalIn: glassOuterBottomRect)
        edgeColor.setStroke()
        glassOuterBottomPath.lineWidth = glassOuterBottomWidth
        glassOuterBottomPath.stroke()
        glassOuterBottomEllipseXStart = glassOuterBottomRect.origin.x + self.edgeWidth/2
        glassOuterBottomEllipseXEnd = glassOuterBottomRect.origin.x + glassOuterBottomRect.width - self.edgeWidth/2
        
        
        //// Glass Outer Top Drawing
        let glassOuterTopRect = CGRect(x: glass.minX + fastFloor(glass.width * 0.00169 + 0.46) + 0.04, y: glass.minY + fastFloor(glass.height * 0.00000 + 0.5), width: fastFloor(glass.width * 0.98118 + 0.46) - fastFloor(glass.width * 0.00169 + 0.46), height: fastFloor(glass.height * 0.15789 + 0.5) - fastFloor(glass.height * 0.00000 + 0.5))
        let glassOuterTopPath = UIBezierPath(ovalIn: glassOuterTopRect)
        glassTopEllipseYStart = glassOuterTopRect.origin.y
        glassTopEllipseYCenter = glassOuterTopRect.midY
        
        edgeColor.setStroke()
        glassOuterTopPath.lineWidth = glassTopWidth
        glassOuterTopPath.stroke()
        
        
        //// Outer Left Edge Drawing
        let outerLeftEdgePath = UIBezierPath()
        outerLeftEdgePath.move(to: CGPoint(x: glass.minX + 0.00000 * glass.width, y: glass.minY + 0.92814 * glass.height))
        outerLeftEdgePath.addLine(to: CGPoint(x: glass.minX + 0.00077 * glass.width, y: glass.minY + 0.08163 * glass.height))
        edgeColor.setStroke()
        outerLeftEdgePath.lineWidth = lineWidth
        outerLeftEdgePath.stroke()
        
        
        //// Outer Right Edge Drawing
        let outerRightEdgePath = UIBezierPath()
        outerRightEdgePath.move(to: CGPoint(x: glass.minX + 0.98286 * glass.width, y: glass.minY + 0.92814 * glass.height))
        outerRightEdgePath.addLine(to: CGPoint(x: glass.minX + 0.98213 * glass.width, y: glass.minY + 0.08211 * glass.height))
        edgeColor.setStroke()
        outerRightEdgePath.lineWidth = lineWidth
        outerRightEdgePath.stroke()
        
        //// Inner Structure Group
        //// Glass Inner Bottom Drawing
        let glassInnerBottomRect = CGRect(x: glass.minX + fastFloor(glass.width * 0.07703 + 0.46) + 0.04, y: glass.minY + fastFloor(glass.height * 0.80702 + 0.5), width: fastFloor(glass.width * 0.90583 + 0.46) - fastFloor(glass.width * 0.07703 + 0.46), height: fastFloor(glass.height * 0.95614) - fastFloor(glass.height * 0.80702 + 0.5) + 0.5)
        let glassInnerBottomPath = UIBezierPath(ovalIn: glassInnerBottomRect)
        context.saveGState()
        context.setShadow(offset: helpShadowOvalShadow.shadowOffset, blur: helpShadowOvalShadow.shadowBlurRadius, color: (helpShadowOvalShadow.shadowColor as! UIColor).cgColor)
        glassBottomEllipseYCenter = glassInnerBottomRect.midY
        glassInnerBottomEllipseXStart = glassInnerBottomRect.origin.x
        glassInnerBottomEllipseXEnd = glassInnerBottomRect.origin.x + glassInnerBottomRect.width
        glassInnerBottomEllipseHeight = glassInnerBottomRect.size.height
        
        fluidColor(_currentProgressPercentage).setFill()
        if _currentProgressPercentage > 0 { glassInnerBottomPath.fill() }
        
        context.restoreGState()
        
        context.saveGState()
        context.setShadow(offset: shadow.shadowOffset, blur: shadow.shadowBlurRadius, color: (shadow.shadowColor as! UIColor).cgColor)
        edgeColor.setStroke()
        glassInnerBottomPath.lineWidth = glassOuterBottomWidth
        glassInnerBottomPath.stroke()
        context.restoreGState()
        
        
        //// Inner Left Edge Drawing
        let innerLeftEdgePath = UIBezierPath()
        innerLeftEdgePath.move(to: CGPoint(x: glass.minX + 0.07478 * glass.width, y: glass.minY + 0.88144 * glass.height))
        innerLeftEdgePath.addLine(to: CGPoint(x: glass.minX + 0.00026 * glass.width, y: glass.minY + 0.08140 * glass.height))
        edgeColor.setStroke()
        innerLeftEdgePath.lineWidth = lineWidth
        innerLeftEdgePath.stroke()
        
        
        //// Inner Right Edge Drawing
        let innerRightEdgePath = UIBezierPath()
        innerRightEdgePath.move(to: CGPoint(x: glassInnerBottomEllipseXEnd, y: glassInnerBottomRect.midY))
        innerRightEdgePath.addLine(to: CGPoint(x: glassOuterTopRect.width + glassOuterTopRect.origin.x, y: glassOuterTopRect.midY))
        
        edgeColor.setStroke()
        innerRightEdgePath.lineWidth = lineWidth
        innerRightEdgePath.stroke()
    }
}

