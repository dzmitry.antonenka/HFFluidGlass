Pod::Spec.new do |s|

    s.name         = "HFFluidGlass"
    s.version      = "1.0.3"
    s.summary      = "HFFluidGlass is simple vector drawn view to display glass with fluid portion."

    s.description  = "HFFluidGlass is simple vector drawn view to display glass with fluid portion. Can be used in any iOS app. required iOS version 10.0+"

    s.homepage     = "http://healthandfood.azurewebsites.net"
    s.license = { :type => 'MIT', :file => 'MIT-LICENSE.txt' }

    s.author             = "schurik77799@gmail.com"

    s.platform     = :ios, "9.0"
    s.source       = { :git => "https://dzmitry.antonenka@gitlab.com/dzmitry.antonenka/HFFluidGlass.git", :tag => "1.0.3" }

    s.source_files  = 'HFFluidGlass/**/*.{swift}'
end
